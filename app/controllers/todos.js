var _ = require('underscore')
	
require('!style!css!sass!./../stylesheets/todo.scss')

module.exports = function($scope, $http){

	$scope.pending = true

	$http.get('/api/todos')
		.success(function(res){
			$scope.todos = res.todos
			$scope.pending = false
		})

	$scope.toggle = function(id){

		var todo = _.findWhere($scope.todos, { id : id })

		todo.pending = true

		$http.put('/api/todos/' + id, { done : !todo.done }).success(function(){
			todo.done = !todo.done	
			todo.pending = false		
		})
		
	}
}