var angular = require('angular')

var app = angular.module('myApp', [
	require('angular-route')
]).config(function($routeProvider){
	$routeProvider
		.when("/todos", {
			templateUrl : 'todos.html',
			controller  : 'todos'
		})
		.when("/todos/:todoId", {
			templateUrl : 'todo.html',
			controller  : 'todo'
		})
		.when("/help", {
			templateUrl : 'help.html',
			controller : 'help'
		})
})

app.controller('todos', require('./controllers/todos'))

app.controller('todo', function($scope, $routeParams){
	$scope.todoId = $routeParams.todoId
})

app.controller('help', function(){
	console.log('help controller')
})