var express = require('express');
var _ = require('underscore')

var app = express();

app.use(express.static('assets'));

var webpackDevMiddleware = require("webpack-dev-middleware");
var webpack = require("webpack");
var webpackConfig = require('./webpack.config.js')
var compiler = webpack(_.extend(webpackConfig, { output: { path: '/',  }} ));

app.use(webpackDevMiddleware(compiler, {
  contentBase: __dirname + "/app",
  stats: { colors: true }
}));

app.get('/api/todos', function(req, res){
	setTimeout(function(){
		res.send({ todos : [
			{ id : 1, name : 'learn angular', done : false },
			{ id : 2, name : 'setup weback with angular', done : false },
			{ id : 3, name : 'setting up requirements', done : false }
		]})
	}, 1000)
})

app.put('/api/todos/:id', function(req, res){
	setTimeout(function(){
		res.status(200).send({})
	}, 1000)
})

var server = app.listen(3000)
console.log('server started')
